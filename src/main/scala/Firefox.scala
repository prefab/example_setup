import prefab.prelude.plan.*
import prefab.contrib.firefox.user_js_settings.*
import prefab.contrib.firefox.user_chrome_css.*


case class Firefox() extends Plan {

	def ensure(host: LocalHost) = {

		host.ensure(UserJsSettings(
			host.userDir,
			"browser.aboutConfig.showWarning" -> false,
			"browser.quitShortcut.disabled" -> true, //disable Ctrl+Q
			"browser.tabs.closeWindowWithLastTab" -> false,
			"extensions.pocket.enabled" -> false,
			"toolkit.legacyUserProfileCustomizations.stylesheets" -> true, //enable userChrome.css loading
                        "widget.gtk.overlay-scrollbars.enabled" -> false, //always show scrollbars
		))


		host.ensure(UserChromeCss(
			host.userDir,
			"""
			|/* Title bar */
			|.titlebar-buttonbox {
			|	display: none !important;
			|}
			|
			|.titlebar-spacer {
			|	display: none !important;
			|}
			|
			|/* Tab bar */
			|#TabsToolbar {
			|	margin-left: calc(15vw + 25em - 1px) !important;
			|}
			|
			|/* Nav bar*/
			|#nav-bar {
			|	background: transparent !important;
			|	margin-top: calc(-1 * (var(--tab-min-height) + var(--inline-tab-padding)));
			|	box-shadow: none !important;
			|	margin-right: calc(85vw - 25em) !important;
			|	display: flex;
			|}
			|#nav-bar-customization-target {
			|	order: 1;
			|	flex-grow: 1;
			|}
			|
			|/* URL bar */
			|#back-button {
			|	display: none !important;
			|}
			|
			|#forward-button {
			|	display: none !important;
			|}
			|
			|#tracking-protection-icon-container {
			|	display: none !important;
			|}
			|
			|#urlbar-container {
			|	min-width: 200px !important;
			|	margin-right: 0 !important;
			|}
			|
			|#urlbar {
			|	--urlbar-height: var(--urlbar-container-height) !important;
			|	background: transparent !important;
			|	border: none !important;
			|	box-shadow: none !important;
			|}
			|#urlbar-background {
			|	border-radius: 0 !important;
			|}
			|#notification-popup-box {
			|	height: calc(var(--urlbar-container-height) - 2 * var(--urlbar-container-padding)) !important;
			|}
			|
			|#PanelUI-button {
			|	display: none !important;
			|}
			|""".stripMargin
		))


		{ //create .desktop file with --new-window
			val referenceFileLines = host.file("/usr/share/applications/firefox.desktop").lines

			val newFileLines = referenceFileLines.map { line =>
				if (line == "Exec=firefox %u") {
					"Exec=firefox --new-window %u"
				} else {
					line
				}
			}

			if (referenceFileLines != newFileLines) {
				host.userDir.file(".local/share/applications/firefox.desktop")
					.ensure(_.textIs(newFileLines.mkString("\n")))
			}
		}
	}
}
