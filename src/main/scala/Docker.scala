import prefab.prelude.config.*
import prefab.prelude.plan.*
import prefab.tech.pkg.packages.*


object Docker {
	
	private enum MountType {
		case Bind
	}
	private case class Mount(mountType: MountType, source: Path, target: String)
	
	
	case object Run {
		def apply(image: String): Run = Run(
			image = image,
			name = None,
			_rm = false,
			_detach = false,
			mounts = Seq.empty,
			publishedPorts = Seq.empty,
			envs = Seq.empty,
			extraArgs = Seq.empty,
		)
	}
	case class Run private (
		private val image: String,
		private val name: Option[String],
		private val _rm: Boolean,
		private val _detach: Boolean,
		private val mounts: Seq[Mount],
		private val publishedPorts: Seq[(Int, Int)],
		private val envs: Seq[(String, String)],
		private val extraArgs: Seq[String],
	) extends Plan {
		
		def name(name: String): Run = this.copy(name=Some(name))
		def extraArg(arg: String): Run = this.copy(extraArgs = this.extraArgs.appended(arg))
		
		
		def detach(): Run = this.copy(_detach=true)
		
		def env(name: String, value: String): Run = this.copy(envs = this.envs.appended((name, value)))
		
		def mountBind(source: Path, target: String): Run = this.copy(mounts = this.mounts.appended(Mount(MountType.Bind, source, target)))
		
		def publish(hostPort: Int, containerPort: Int): Run = this.copy(publishedPorts = this.publishedPorts.appended((hostPort, containerPort)))
		
		def rm(): Run = this.copy(_rm=true)
		
		
		def ensure(host: LocalHost) = {
			val command = StringBuilder("docker run")
			
			name.foreach(name => command.append(s" --name $name"))
			
			if (_detach) {
				command.append(s" --detach")
			}
			
			if (envs.nonEmpty) {
				command.append(" ")
				command.append(
					envs
						.map((name, value) => s"--env $name=$value")
						.mkString(" ")
				)
			}
			
			if (mounts.nonEmpty) {
				command.append(" ")
				command.append(
					mounts.map {
						case Mount(mountType, source, target) => s"--mount type=$mountType,source=$source,target=$target"
					}
					.mkString(" ")
				)
			}
			
			if (publishedPorts.nonEmpty) {
				command.append(" ")
				command.append(
					publishedPorts
						.map((hostPort, containerPort) => s"--publish $hostPort:$containerPort")
						.mkString(" ")
				)
			}
			
			if (_rm) {
				command.append(s" --rm")
			}
			
			if (extraArgs.nonEmpty) {
				command.append(" ")
				command.append(extraArgs.mkString(" "))
			}
			
			command.append(s" $image")
			
			host.ensure(Command(command.toString()))
		}
	}
}
