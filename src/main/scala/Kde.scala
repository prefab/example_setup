import prefab.prelude.plan.*
import prefab.contrib.kde.kwriteconfig5.*
import prefab.contrib.kde.kreadconfig5.*
import prefab.tech.pkg.packages.*


case class Kde() extends Plan {
	
	def ensure(host: LocalHost) = {
		
		host.ensure(Packages.Installed(
			"dolphin-plugins", "kcolorchooser", "kruler"
		))
		
		
		
		val configDir = host.userDir.dir(".config")
		
		
		{
			val kactivitymanagerdrc = configDir.file("kactivitymanagerdrc")
			
			host.ensure(KWriteConfig5(
				kactivitymanagerdrc,
				Group("Plugins"),
				"org.kde.ActivityManager.VirtualDesktopSwitchEnabled" -> "true",
			))
		}
		
		{
			val kdeglobals = configDir.file("kdeglobals")
			
			host.ensure(KWriteConfig5(
				kdeglobals,
				Group("KDE"),
				"AnimationDurationFactor" -> "0.250000",
			))
		}
		
		{
			val kglobalshortcutsrc = configDir.file("kglobalshortcutsrc")
			
			host.ensure(KWriteConfig5(
				kglobalshortcutsrc,
				Group("kwin"),
				"Switch to Next Desktop"     -> "Meta+S,none,Switch to Next Desktop",
				"Switch to Previous Desktop" -> "Meta+W,none,Switch to Previous Desktop",
				"Window Close"               -> "Meta+Q,Alt+F4,Close Window",
				"Window Fullscreen"          -> "Meta+F11,none,Make Window Fullscreen",
				"Window Operations Menu"     -> "Meta+F3,Alt+F3,Window Operations Menu",
				"Window to Next Desktop"     -> "Meta+Shift+S,none,Window to Next Desktop",
				"Window to Previous Desktop" -> "Meta+Shift+W,none,Window to Previous Desktop",
			))
			
			host.ensure(KWriteConfig5(
				kglobalshortcutsrc,
				Group("org.kde.krunner.desktop"),
				"_launch" -> "Meta+Esc,none,KRunner",
			))
			
			host.ensure(KWriteConfig5(
				kglobalshortcutsrc,
				Group("plasmashell"),
				"manage activities"       -> "none,Meta+Q,Show Activity Switcher",
				"stop current activity"   -> "none,Meta+S,Stop Current Activity",
				"switch to next activity" -> "Meta+Ctrl+Tab,none,Switch to Next Activity",
			))
		}
		
		{
			val kscreenlockerrc = configDir.file("kscreenlockerrc")
			
			host.ensure(KWriteConfig5(
				kscreenlockerrc,
				Group("Daemon"),
				"Timeout" -> "10",
			))
		}
		
		{
			val kwinrc = configDir.file("kwinrc")
			
			host.ensure(KWriteConfig5(
				kwinrc,
				Group("Windows"),
				"DelayFocusInterval"    -> "100",
				"FocusPolicy"           -> "FocusFollowsMouse",
				"NextFocusPrefersMouse" -> "true",
				"Placement"             -> "UnderMouse",
			))
			
			
			host.ensure(KWriteConfig5(
				kwinrc,
				Group("MouseBindings"),
				"CommandAllKey" -> "Meta",
			))
			
			
			host.ensure(KWriteConfig5(
				kwinrc,
				Group("Plugins"),
				"diminactiveEnabled" -> "true",
			))
			host.ensure(KWriteConfig5(
				kwinrc,
				Group("Effect-DimInactive"),
				"Strength" -> "15",
			))
			
			
			host.ensure(KWriteConfig5(
				kwinrc,
				Group("TabBox"),
				"BorderActivate" -> "9",
			))
			host.ensure(KWriteConfig5(
				kwinrc,
				Group("Effect-PresentWindows"),
				"BorderActivateAll" -> "9",
			))
			
			
			host.ensure(KWriteConfig5(
				kwinrc,
				Group("Desktops"),
				"Id_1"   -> "a7808672-2f2f-4fa1-8be1-1b8c9bf364cc",
				"Id_2"   -> "3926b3ec-028b-44b9-a017-0e63aeecefb3",
				"Id_3"   -> "0f33684c-7a8f-4021-812e-e23258511512",
				"Id_4"   -> "f70655e9-a464-4cde-8b10-ba411450d069",
				"Id_5"   -> "f87fc177-8f58-4ba6-936e-3713c3bba35b",
				"Id_6"   -> "23a78a4e-5e8c-40e9-885b-47d48fa5b347",
				"Id_7"   -> "92b0562f-b0a5-473f-b5ac-60d07dac30a8",
				"Id_8"   -> "2686550c-fd09-4fc5-88fc-58ca30dad4a4",
				"Id_9"   -> "77f83992-1f69-4b7b-aac7-664bdcbdcb72",
				"Id_10"  -> "66cdac6f-1889-42b2-9488-f10107339204",
				"Id_11"  -> "bd43ef7d-1dea-4f99-be25-16a687cb9c89",
				"Id_12"  -> "b1bb377b-0f35-4eb7-a9cc-e2e05a62a8a4",
				"Id_13"  -> "aaabcc7b-4225-4428-95ff-2ec2f68e656a",
				"Id_14"  -> "7b964b79-592f-4ee7-aa52-1d881886ee35",
				"Id_15"  -> "8cc4f4c2-0ab0-4fd1-b272-159cef7059f4",
				"Id_16"  -> "47e2b4e7-ccdf-427a-a67c-f7e53f5a6898",
				"Id_17"  -> "b730b74d-dc38-4325-bd41-a4eb5e0bb0a5",
				"Id_18"  -> "3664de3e-e7e5-43ca-b457-8fa25daed12d",
				"Id_19"  -> "41acb8f5-961f-43c6-ac7f-3dd3562634d1",
				"Id_20"  -> "6b1fa72a-346c-4b9a-b72b-bd1abb50ce7d",
				"Number" -> "20",
				"Rows"   -> "20",
			))
		}
		
		{
			val kxkbrc = configDir.file("kxkbrc")
			
			val keyboardOptions = host.check(KReadConfig5(
				kxkbrc,
				Group("Layout"),
				"Options",
			))
			
			if (keyboardOptions.contains("caps:none") == false) {
				
				host.ensure(KWriteConfig5(
					kxkbrc,
					Group("Layout"),
					"Options" -> s"$keyboardOptions,caps:none",
				))
			}
		}
		
		{
			val plasmanotifyrc = configDir.file("plasmanotifyrc")
			
			host.ensure(KWriteConfig5(
				plasmanotifyrc,
				Group("Notifications"),
				"PopupPosition" -> "BottomRight",
			))
		}
		
		
		
		{ // set up Krohnkite
			val kglobalshortcutsrc = configDir.file("kglobalshortcutsrc")
			
			host.ensure(KWriteConfig5(
				kglobalshortcutsrc,
				Group("kwin"),
				"Krohnkite: Down/Next"  -> "Meta+D,none,",
				"Krohnkite: Up/Prev"    -> "Meta+A,none,",
				"Krohnkite: Float"      -> "Meta+U,none,",
				"Krohnkite: Set master" -> "Meta+Return,none,",
			))
		}
		
		
		{ //create .desktop file for Kate with --new
			val referenceFileLines = host.file("/usr/share/applications/org.kde.kate.desktop").lines
			
			val newFileLines = referenceFileLines.map { line =>
				if (line == "Exec=kate -b %U") {
					"Exec=kate --new %U"
				} else {
					line
				}
			}
			
			if (referenceFileLines != newFileLines) {
				host.userDir.file(".local/share/applications/org.kde.kate.desktop")
					.ensure(_.textIs(newFileLines.mkString("\n")))
			}
		}
	}
}
