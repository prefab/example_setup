import prefab.prelude.plan.*
import prefab.tech.pkg.packages.*



case class Terminal() extends Plan {
	
	def ensure(host: LocalHost) = {
		
		host.ensure(Packages.Installed(
			"fish", "tree", "gcc"
		))
		
		
		{ //Git
			host.ensure(Packages.Installed(
				"git", "bat"
			))
			
			host.ensure(Command(
				s"""git config --global core.pager "bat --paging always --theme=ansi""""
			))
		}
		
		
		{ //Starship
			val starshipScript = host.tempDir.file("starship.sh")
			
			starshipScript.logIfChanged {
				host.ensure(Command(
					s"""curl --no-progress-meter --output $starshipScript --location https://starship.rs/install.sh"""
				))
				host.ensure(Command(
					s"""chmod u+x $starshipScript"""
				))
				host.ensure(Command(
					s"""$starshipScript --yes"""
				))
				host.ensure(Command(
					s"""rm $starshipScript"""
				))
			}
			
			host.userDir
				.dir(".config")
				.dir("fish")
				.file("config.fish")
				.ensure(
					_.containsLine("starship init fish | source")
				)
			
			
			host.userDir
				.dir(".config")
				.file("starship.toml")
				.ensure(_.textIs(
					"""
					|[time]
					|disabled = false
					|style = "purple"
					|
					|[cmd_duration]
					|style = "purple"
					|
					|[rust]
					|disabled = false
					|
					|[directory]
					|truncate_to_repo = false
					|truncation_length = -1
					|
					|[scala]
					|disabled = true
					|""".stripMargin
				))
		}
	}
}
