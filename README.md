# Prefab Example Setup

This repository provides an example for using Prefab to configure a KDE Plasma desktop and Firefox.

It is an actual setup, so it may be harder to follow, but can provide guidance on how to structure more complex setups.


## How To Run
You probably don't actually want to run this project on your machine, as it will make various changes.  
However this documents how any Prefab project would be run.

1. Set up Scala and SBT on your machine
We recommend using Coursier for this: https://get-coursier.io/docs/cli-installation 

2. Make Prefab available as a local dependency
Currently, Prefab is not yet available in one of the well-known artifact repositories, so this extra step is necessary to run the setup.  
First, clone the Prefab repository from here: https://codeberg.org/prefab/prefab  
Then run `sbt publishLocal` in the cloned folder.

3. Start the deployment
With this project cloned, run `sbt run` in the project directory.

